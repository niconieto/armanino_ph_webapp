from django.shortcuts import render
from django.http import HttpResponse
import requests
from .models import PictureLink

from .models import Greeting


# Create your views here.
def index(request):
    template = 'index.html'
    context = {"img_links": PictureLink.objects.all()}
    return render(request, template, context)


def db(request):

    greeting = Greeting()
    greeting.save()

    greetings = Greeting.objects.all()

    return render(request, 'db.html', {'greetings': greetings})

