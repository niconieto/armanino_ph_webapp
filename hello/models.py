from django.db import models


# Create your models here.
class Greeting(models.Model):
    when = models.DateTimeField('date created', auto_now_add=True)


class PictureLink(models.Model):
    url = models.CharField(max_length=65535)

    def __str__(self):
        return self.url

