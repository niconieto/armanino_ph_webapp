# Generated by Django 2.0.3 on 2018-03-20 03:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hello', '0002_auto_20180320_0318'),
    ]

    operations = [
        migrations.CreateModel(
            name='PictureLink',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.CharField(max_length=65535)),
            ],
        ),
    ]
